import { Component, OnInit } from "@angular/core";
import { listAnimation, staggerdFadeIn } from "src/app/animations";

@Component({
	selector: "app-reset",
	templateUrl: "./reset.component.html",
	styleUrls: ["./reset.component.scss"],
	animations: [staggerdFadeIn, listAnimation],
})
export class ResetComponent implements OnInit {
	hide: boolean = true;
	hide2: boolean = true;

	items: number[] = [];

	showItems() {
		this.items = [0, 1, 2, 3, 4];
	}

	hideItems() {
		this.items = [];
	}

	push() {
		// this.items.length ? this.hideItems() : this.showItems();
		this.items.push(1);
	}
	pop() {
		// this.items.length ? this.hideItems() : this.showItems();
		this.items.pop();
	}

	constructor() {}

	ngOnInit(): void {
		setTimeout(() => {
			this.items = [1, 2, 3, 4, 5, 6];
		});
	}
}
