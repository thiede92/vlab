import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { AuthService } from "src/app/core/services/auth/auth.service";
import { untilDestroyed } from "@ngneat/until-destroy";

@Component({
	selector: "app-register",
	templateUrl: "./register.component.html",
	styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit {
	hidePassword: boolean = true;
	mainInvalidMessageVisibility: boolean = false;
	mainInvalidMessageValue: string = "";
	loading: boolean = false;
	emailRegex = new RegExp("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$");
	registerUserForm = this.fb.group({
		email: ["", [Validators.required, Validators.pattern(this.emailRegex)]],
		password: ["", Validators.required],
		firstName: ["", Validators.required],
		lastName: ["", Validators.required],
		address: [""],
		city: ["", Validators.required],
		postCode: [""],
		albumNumber: ["", Validators.required],
	});

	constructor(
		private readonly fb: FormBuilder,
		private readonly auth: AuthService,
		private readonly router: Router,
	) { }

	ngOnInit(): void { }

	registerUser(user: FormGroup) {
		this.mainInvalidMessageVisibility = false;
		this.mainInvalidMessageValue = "";

		if (!user.valid) {
			for (const [key, value] of Object.entries(this.registerUserForm.value)) {
				this.registerUserForm.get(key)?.markAsTouched();
			}
		} else if (user.valid) {
			this.loading = !this.loading;
			this.auth
				.registerUser(user.value)
				.pipe(untilDestroyed(this))
				.subscribe({
					next: () => {
						this.auth.setRegisterSuccessGuard(true);
						this.router.navigate(["/register-success"]);
					},
					error: error => {
						if (error.error === "Email is used") {
							this.mainInvalidMessageValue = "error.provided-email-is-already-in-use";
							this.mainInvalidMessageVisibility = true;
						} else {
							this.mainInvalidMessageValue =
								"error.an-error-has-occured-please-try-again-in-a-while";
							this.mainInvalidMessageVisibility = true;
						}
						this.loading = !this.loading;
					},
				});
		}
	}

	getErrorMessage(controlName: string) {
		const errorType: any = this.registerUserForm.get(controlName)?.errors;
		for (let [key, value] of Object.entries(errorType)) {
			if (key === "required") {
				return "error.this-field-is-required";
			} else if (key === "pattern" && controlName === "email") {
				return "error.provided-email-has-an-incorrect-format";
			}
		}
		return "";
	}
}
