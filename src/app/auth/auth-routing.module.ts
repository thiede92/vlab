import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { RegisterSuccessGuard } from "../core/guards/register-success-guard/register-success.guard";
import { AuthComponent } from "./auth.component";
import { LoginComponent } from "./login/login.component";
import { RecoveryComponent } from "./recovery/recovery.component";
import { RegisterSuccessComponent } from "./register-success/register-success.component";
import { RegisterComponent } from "./register/register.component";
import { ResetComponent } from "./reset/reset.component";

const routes: Routes = [
	{
		path: "",
		component: AuthComponent,
		children: [
			{
				path: "",
				redirectTo: "login",
				pathMatch: "prefix",
			},
			{
				path: "login",
				component: LoginComponent,
				data: { animation: "" },
			},
			{
				path: "register",
				component: RegisterComponent,
				data: { animation: "" },
			},
			{
				path: "register-success",
				component: RegisterSuccessComponent,
				data: { animation: "" },
				canActivate: [RegisterSuccessGuard],
			},
			// {
			// 	path: "recovery",
			// 	component: RecoveryComponent,
			// 	data: { animation: "" },
			// },
			// {
			// 	path: "reset",
			// 	component: ResetComponent,
			// 	data: { animation: "" },
			// },
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class AuthRoutingModule {}
