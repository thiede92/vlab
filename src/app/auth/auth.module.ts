import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AuthRoutingModule } from "./auth-routing.module";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { RecoveryComponent } from "./recovery/recovery.component";
import { ResetComponent } from "./reset/reset.component";
import { SharedModule } from "../shared/shared.module";
import { AuthComponent } from "./auth.component";
import { ReactiveFormsModule } from "@angular/forms";
import { RegisterSuccessComponent } from "./register-success/register-success.component";
import { AuthToolbarComponent } from "../core/components/auth-toolbar/auth-toolbar.component";

@NgModule({
	declarations: [
		LoginComponent,
		RegisterComponent,
		RecoveryComponent,
		ResetComponent,
		AuthComponent,
		RegisterSuccessComponent,
		AuthToolbarComponent,
	],
	imports: [CommonModule, AuthRoutingModule, SharedModule],
})
export class AuthModule {}
