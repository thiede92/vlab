import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { AuthService } from "src/app/core/services/auth/auth.service";
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';

@UntilDestroy()
@Component({
	selector: "app-login",
	templateUrl: "./login.component.html",
	styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
	hidePassword: boolean = true;
	loading: boolean = false;
	mainInvalidMessageVisibility: boolean = false;
	mainInvalidMessageValue: string = "";

	loginUserForm = this.fb.group({
		email: ["", Validators.required],
		password: ["", Validators.required],
	});

	constructor(
		private readonly authService: AuthService,
		private router: Router,
		private readonly fb: FormBuilder,
	) { }

	ngOnInit(): void { }

	loginUser(user: FormGroup) {
		this.mainInvalidMessageVisibility = false;
		this.mainInvalidMessageValue = "";

		if (!user.valid) {
			for (const [key, value] of Object.entries(this.loginUserForm.value)) {
				this.loginUserForm.get(key)?.markAsTouched();
			}
		} else if (user.valid) {
			this.loading = !this.loading;
			this.authService
				.logInUser(user.value)
				.pipe(untilDestroyed(this))
				.subscribe({
					next: (data: any) => {
						this.authService.setToken(data.access_token);
						this.router.navigateByUrl("/dashboard").finally();
					},
					error: error => {
						if (error.error === "User doesn't exist") {
							this.mainInvalidMessageValue = "error.invalid-email-or-password";
							this.mainInvalidMessageVisibility = true;
						} else {
							this.mainInvalidMessageValue =
								"error.an-error-has-occured-please-try-again-in-a-while";
							this.mainInvalidMessageVisibility = true;
						}
						this.loading = !this.loading;
					},
				});
		}
	}

	getErrorMessage(controlName: string) {
		const control = this.loginUserForm.get(controlName);
		if (control?.errors) {
			return "error.this-field-is-required";
		} else {
			return "";
		}
	}
}
