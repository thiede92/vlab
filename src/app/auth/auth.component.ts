import { Component, OnInit } from "@angular/core";
import { ChildrenOutletContexts } from "@angular/router";
import { slideInAnimation } from "../animations";

@Component({
	selector: "app-auth",
	templateUrl: "./auth.component.html",
	styleUrls: ["./auth.component.scss"],
	animations: [slideInAnimation],
})
export class AuthComponent implements OnInit {
	constructor(private contexts: ChildrenOutletContexts) {}

	ngOnInit(): void {}

	getRouteAnimationData() {
		return this.contexts.getContext("primary")?.route?.snapshot?.data?.["animation"];
	}
}
