export enum USER_ROLES {
	admin = "admin",
	teacher = "teacher",
	student = "student",
}
