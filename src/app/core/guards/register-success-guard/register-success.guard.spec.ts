import { TestBed } from "@angular/core/testing";

import { RegisterSuccessGuard } from "./register-success.guard";

describe("RegisterSuccessGuard", () => {
	let guard: RegisterSuccessGuard;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		guard = TestBed.inject(RegisterSuccessGuard);
	});

	it("should be created", () => {
		expect(guard).toBeTruthy();
	});
});
