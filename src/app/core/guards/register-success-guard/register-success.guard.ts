import { Injectable } from "@angular/core";
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
	UrlTree,
} from "@angular/router";
import { Observable } from "rxjs";

import { AuthService } from "../../services/auth/auth.service";

@Injectable({
	providedIn: "root",
})
export class RegisterSuccessGuard implements CanActivate {
	constructor(private router: Router, private readonly authService: AuthService) {}

	canActivate(
		route: ActivatedRouteSnapshot,
		state: RouterStateSnapshot,
	): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
		const canRedirect = this.authService.getRegisterSuccessGuard();

		if (canRedirect) {
			this.authService.setRegisterSuccessGuard(false);
			return true;
		} else {
			return this.router.parseUrl("/login");
		}
	}
}
