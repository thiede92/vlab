import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { JwtHelperService } from "@auth0/angular-jwt";
import { Router } from "@angular/router";
import { fakeApiUrl, url } from "../../api-config";
import { IUser } from "../../models/user";
import { UntilDestroy } from "@ngneat/until-destroy";
import { CookieService } from "ngx-cookie-service";
import { ITokenData } from "../../models/token-data";
import * as CryptoJS from "crypto-js";

@UntilDestroy()
@Injectable({
	providedIn: "root",
})
export class AuthService {
	private cookie_token = "token";
	url = url;
	faUrl = fakeApiUrl;
	prefix = "auth";
	jwtHelper = new JwtHelperService();
	RegisterSuccessGuard: boolean = false;

	constructor(
		private readonly http: HttpClient,
		private readonly router: Router,
		private readonly cookieService: CookieService,
	) {}

	isAuthenticated() {
		const token = this.cookieService.get(this.cookie_token);

		if (!token) return false;

		if (this.isTokenExpired(token)) {
			this.destroyToken();
			return false;
		}

		const tokenData: ITokenData = this.jwtHelper.decodeToken(token);

		return tokenData._doc.role.map(role => {
			return role === "student" || role === "teacher" || role === "admin";
		});
	}

	generateToken(email: string, role: string) {
		const iat = (Date.now() / 1000).toFixed();
		const exp = ((Date.now() + 10000) / 1000).toFixed();

		const header = {
			alg: "HS256",
			typ: "JWT",
		};

		const payload = {
			email: email,
			role: role,
			iat: iat,
			exp: exp,
		};

		const stringifiedHeader = CryptoJS.enc.Utf8.parse(JSON.stringify(header));
		const encodedHeader = this.base64url(stringifiedHeader);

		const stringifiedData = CryptoJS.enc.Utf8.parse(JSON.stringify(payload));
		const encodedData = this.base64url(stringifiedData);

		const token = encodedHeader + "." + encodedData;
		const secret = "eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ";

		let signature: any = CryptoJS.HmacSHA256(token, secret);
		signature = this.base64url(signature);

		const signedToken = token + "." + signature;
		return signedToken;
	}

	base64url(source: any) {
		let encodedSource = CryptoJS.enc.Base64.stringify(source);

		encodedSource = encodedSource.replace(/=+$/, "");

		encodedSource = encodedSource.replace(/\+/g, "-");
		encodedSource = encodedSource.replace(/\//g, "_");

		return encodedSource;
	}

	setToken(token: string) {
		this.cookieService.set(this.cookie_token, token);
	}

	getToken() {
		return this.cookieService.get(this.cookie_token);
	}

	isTokenExpired(token: string) {
		return this.jwtHelper.isTokenExpired(token);
	}

	destroyToken() {
		this.cookieService.delete(this.cookie_token);
	}

	logInUser(user: Pick<IUser, "email" | "password">) {
		return this.http.post(`${this.url}/${this.prefix}/login`, user);
	}

	getProfile() {
		return this.http.get<IUser>(`${this.url}/${this.prefix}/profile`);
	}

	logOutUser() {
		this.destroyToken();
		this.router.navigateByUrl("/login").finally();
	}

	registerUser(user: any) {
		return this.http.get(`${this.faUrl}/users?email=${user.email}`);
		// return this.http.post(`${this.url}/${this.prefix}/register`, user);
	}

	setRegisterSuccessGuard(value: boolean) {
		this.RegisterSuccessGuard = value;
	}

	getRegisterSuccessGuard() {
		return this.RegisterSuccessGuard;
	}

	updateUser(user: IUser, id?: string) {
		delete user.id;

		return this.http.patch<IUser>(`${this.url}/users/${id}`, {
			...user,
		});
	}

	createUser(user: any) {
		user.role = "student";
		user.id = Date.now();
		return this.http.post(`${this.faUrl}/users`, user);
	}
}
