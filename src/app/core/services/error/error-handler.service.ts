import { Injectable } from "@angular/core";
import { HttpErrorResponse } from "@angular/common/http";
import { EVlabErrorMessages } from "../../enums/error-messages.enum";
import { Subject, throwError } from "rxjs";

@Injectable({
	providedIn: "root",
})
export class ErrorHandlerService {
	hasError$: Subject<boolean> = new Subject<boolean>();
	handleError(errRes: HttpErrorResponse) {
		let errorMessage = EVlabErrorMessages.DEFAULT;

		if (!errRes) return throwError(() => errorMessage);

		switch (errRes.status) {
			case 0:
				errorMessage = EVlabErrorMessages.DEFAULT;
				break;
			case 404:
				errorMessage = EVlabErrorMessages.DEFAULT; //404 not found
				break;
			case 500:
				errorMessage = EVlabErrorMessages.DEFAULT; //internal server error
				break;
			default:
				errorMessage = EVlabErrorMessages.DEFAULT;
				break;
		}

		return throwError(() => errorMessage);
	}
}
