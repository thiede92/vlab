import { Injectable } from "@angular/core";

@Injectable({
	providedIn: "root",
})
export class CsvDataService {
	SaveAsCSV(data: any) {
		let encodingBOM: string = "\ufeff";
		let replacer: (key: any, value: any) => any;
		replacer = (key: any, value: any) => (value === null ? "" : value);
		const header = Object.keys(data[0]);
		const csv = data.map((row: { [x: string]: any }) => {
			return header
				.map(fieldName => {
					return JSON.stringify(row[fieldName], replacer);
				})
				.join(",");
		});
		csv.unshift(header.join(","));
		let csvArray: any;
		csvArray = csv.join("\r\n");

		const a = document.createElement("a");
		const blob = new Blob([encodingBOM + csvArray], { type: "text/csv; charset=utf8" });
		const url = window.URL.createObjectURL(blob);

		a.href = url;
		a.download = "myFile.csv";
		a.click();
		window.URL.revokeObjectURL(url);
		a.remove();
	}
}
