import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SidebarComponent } from "./components/sidebar/sidebar.component";
import { AuthToolbarComponent } from "./components/auth-toolbar/auth-toolbar.component";
import { AppToolbarComponent } from "./components/app-toolbar/app-toolbar.component";
import { BreadcrumbsComponent } from "./components/breadcrumbs/breadcrumbs.component";
import { MaterialModule } from "../shared/material/material.module";
import { SharedModule } from "../shared/shared.module";
import { SidebarAccountViewComponent } from "./components/sidebar/sidebar-account-view/sidebar-account-view.component";
import { SidebarSettingsViewComponent } from "./components/sidebar/sidebar-settings-view/sidebar-settings-view.component";

@NgModule({
	declarations: [
		AuthToolbarComponent,
		AppToolbarComponent,
		BreadcrumbsComponent,
		SidebarAccountViewComponent,
		SidebarSettingsViewComponent,
	],
	imports: [CommonModule, MaterialModule, SharedModule],
})
export class CoreModule {}
