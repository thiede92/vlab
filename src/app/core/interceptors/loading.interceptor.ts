import { Injectable } from "@angular/core";
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from "@angular/common/http";
import { delay, finalize, Observable } from "rxjs";
import { LoadingService } from "../services/loading/loading.service";

@Injectable()
export class LoadingInterceptor implements HttpInterceptor {
	constructor(public loadingService: LoadingService) {}
	intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
		this.loadingService.isLoading.next(true);

		return next.handle(request).pipe(
			delay(0),
			finalize(() => {
				this.loadingService.isLoading.next(false);
			}),
		);
	}
}
