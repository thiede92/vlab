import { Component, OnInit } from "@angular/core";
import { NavigationEnd, Router } from "@angular/router";
import { filter, map } from "rxjs";
import { navItems } from "../sidebar/navItems";

@Component({
	selector: "app-breadcrumbs",
	templateUrl: "./breadcrumbs.component.html",
	styleUrls: ["./breadcrumbs.component.scss"],
})
export class BreadcrumbsComponent implements OnInit {
	navItems = navItems;
	breadCrumbs: string = "";

	constructor(private readonly router: Router) {
		router.events
			.pipe(
				filter(event => event instanceof NavigationEnd),
				map((item: any) => {
					let name;
					if (item.url) {
						this.navItems.forEach(navItem => {
							if (navItem.route === item.url) {
								name = navItem.name;
							}
						});
					}
					return name;
				}),
			)
			.subscribe({
				next: (data: any) => {
					this.breadCrumbs = data;
				},
			});
	}

	ngOnInit(): void {}
}
