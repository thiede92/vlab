import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../services/auth/auth.service";

@Component({
	selector: "app-app-toolbar",
	templateUrl: "./app-toolbar.component.html",
	styleUrls: ["./app-toolbar.component.scss"],
})
export class AppToolbarComponent implements OnInit {
	loading = false;
	currentLanguage = "lang-pl";

	constructor(private readonly authService: AuthService) {}

	ngOnInit(): void {}

	logOutUser() {
		this.authService.logOutUser();
	}

	getCurrentLanguageClass() {
		return this.currentLanguage;
	}

	setCurrentLanguage(lang: string) {
		this.currentLanguage = lang;
	}
}
