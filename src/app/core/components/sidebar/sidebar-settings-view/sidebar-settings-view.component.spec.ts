import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SidebarSettingsViewComponent} from './sidebar-settings-view.component';

describe('SidebarSettingsViewComponent', () => {
  let component: SidebarSettingsViewComponent;
  let fixture: ComponentFixture<SidebarSettingsViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarSettingsViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarSettingsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
