export const navItems = [
	{
		name: "dashboard",
		route: "/dashboard",
		icon: "home",
	},
	{
		name: "schedules",
		route: "/schedules",
		icon: "calendar_month",
	},
	{
		name: "groups",
		route: "/groups",
		icon: "groups",
	},
	{
		name: "students",
		route: "/students",
		icon: "person",
	},
	{
		name: "teachers",
		route: "/teachers",
		icon: "school",
	},
	{
		name: "workstations",
		route: "/workstations",
		icon: "workspaces",
	},
	{
		name: "scenarios",
		route: "/scenarios",
		icon: "web_stories",
	},
	{
		name: "measurment-logs",
		route: "/measurment-logs",
		icon: "assignment",
	},
	{
		name: "reports",
		route: "/reports",
		icon: "storage",
	},
	{
		name: "grades",
		route: "/grades",
		icon: "looks_5",
	},
	{
		name: "matlab",
		route: "/matlab",
		icon: "hub",
	},
];
