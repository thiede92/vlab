import {ComponentFixture, TestBed} from '@angular/core/testing';

import {SidebarAccountViewComponent} from './sidebar-account-view.component';

describe('SidebarAccountViewComponent', () => {
  let component: SidebarAccountViewComponent;
  let fixture: ComponentFixture<SidebarAccountViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SidebarAccountViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarAccountViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
