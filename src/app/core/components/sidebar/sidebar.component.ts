import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService} from "../../services/auth/auth.service";
import {navItems} from "./navItems";

@Component({
	selector: "app-sidebar",
	templateUrl: "./sidebar.component.html",
	styleUrls: ["./sidebar.component.scss"],
})
export class SidebarComponent implements OnInit {
	navItems = navItems;

	constructor(private router: Router, private readonly authService: AuthService) {}

	ngOnInit(): void {}

	navigateTo(route: string) {
		this.router.navigateByUrl(route).then();
	}

	isActiveRoute(path: string) {
		const isRouteActive = this.router.isActive(path, true);
		if (isRouteActive) {
			return true;
		} else {
			return false;
		}
	}

	logOutUser() {
		this.authService.logOutUser();
	}

	openSettings() {}

	openMyAccount() {}
}
