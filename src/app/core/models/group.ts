import { IUser } from "./user";

export interface IGroup {
	id?: string;
	name: string;
	fieldOfStudy: string;
	universityGroupCode: string;
	// groupType: string
	// groupStatus: string
	students: IUser[];
}
