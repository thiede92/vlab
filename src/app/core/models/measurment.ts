export interface IMeasurment {
	id: string;
	name: string;
	workstationId: string;
}
