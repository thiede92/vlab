import { IMeasurment } from "./measurment";

export interface IMeasurmentLog {
	id: string;
	name: string;
	measurments: IMeasurment[];
}
