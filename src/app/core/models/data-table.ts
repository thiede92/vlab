export interface IColumnConfig {
	name: string;
	columnName: string;
}
