export interface IGrade {
	id: string;
	gradeLevel: number;
	studentId: string;
	// groupId: string
}
