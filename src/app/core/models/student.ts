export interface IStudent {
  id: string;
	email: string;
	firstName: string;
	lastName: string;
	address: string;
	city: string;
	postCode: string;
	albumNumber: string;
	groups: string;
  isActive?: string;
}
