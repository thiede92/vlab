import { IUser } from "./user";

export interface ITokenData {
	_doc: IUser;
	exp?: number;
	iat: number;
	roles: string[];
	sub: string;
	username: string;
}
