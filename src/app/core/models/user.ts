import { USER_ROLES } from "../enums/user-roles.enum";
import { IGroup } from "./group";

export interface IUser {
	id?: string;
	email: string;
	password?: string;
	firstName: string;
	lastName: string;
	address: string;
	city: string;
	postCode: string;
	albumNumber: string;
	isActive: boolean;
	groups: IGroup[];
	role: USER_ROLES[];
}
