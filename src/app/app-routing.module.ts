import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppGuard } from "./core/guards/app-guard/app.guard";
import { AuthGuard } from "./core/guards/auth-guard/auth.guard";

const routes: Routes = [
	{
		path: "",
		loadChildren: () => import("./auth/auth.module").then(m => m.AuthModule),
		canActivate: [AuthGuard],
	},
	{
		path: "",
		loadChildren: () => import("./modules/modules.module").then(m => m.ModulesModule),
		canActivate: [AppGuard],
	},
	{
		path: "**",
		redirectTo: "dashboard",
	},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
})

export class AppRoutingModule {}
