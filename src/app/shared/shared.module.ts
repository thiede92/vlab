import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {MaterialModule} from "./material/material.module";
import {LayoutModule} from "./layout/layout.module";
import {ComponentsModule} from "./components/components.module";
import {TranslateModule} from "@ngx-translate/core";
import {ReactiveFormsModule} from "@angular/forms";
import {ErrorDirective} from './directives/error.directive';

@NgModule({
	declarations: [
    ErrorDirective,
  ],
	imports: [
		CommonModule,
		MaterialModule,
		LayoutModule,
		ComponentsModule,
		TranslateModule,
		ReactiveFormsModule,
	],
    exports: [
        CommonModule,
        MaterialModule,
        LayoutModule,
        ComponentsModule,
        TranslateModule,
        ReactiveFormsModule,
    ],
})
export class SharedModule {}
