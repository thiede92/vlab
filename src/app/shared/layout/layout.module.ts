import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthLayoutModule } from "./auth-layout/auth-layout.module";
import { AppLayoutModule } from "./app-layout/app-layout.module";

@NgModule({
	declarations: [],
	imports: [CommonModule, AuthLayoutModule, AppLayoutModule],
	exports: [AuthLayoutModule, AppLayoutModule],
})
export class LayoutModule {}
