import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthLayoutComponent } from "./auth-layout.component";
import { MaterialModule } from "../../material/material.module";

@NgModule({
	declarations: [AuthLayoutComponent],
	imports: [CommonModule, MaterialModule],
	exports: [AuthLayoutComponent],
})
export class AuthLayoutModule {}
