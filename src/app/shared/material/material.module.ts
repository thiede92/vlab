import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatCardModule } from "@angular/material/card";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatButtonModule } from "@angular/material/button";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { MatIconModule } from "@angular/material/icon";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatTableModule } from "@angular/material/table";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSelectModule } from "@angular/material/select";
import { MatMenuModule } from "@angular/material/menu";
import { MatChipsModule } from "@angular/material/chips";
import { MatRippleModule } from "@angular/material/core";
import { MatDividerModule } from "@angular/material/divider";
import { MatBadgeModule } from "@angular/material/badge";
import { MatSortModule } from "@angular/material/sort";
import { SkeletonModule } from "primeng/skeleton";
import { MatDialogModule } from "@angular/material/dialog";
import { MatRadioModule } from "@angular/material/radio";

@NgModule({
	declarations: [],
	imports: [CommonModule],
	exports: [
		MatCardModule,
		MatFormFieldModule,
		MatInputModule,
		MatButtonModule,
		MatToolbarModule,
		MatCheckboxModule,
		MatIconModule,
		MatProgressBarModule,
		MatSidenavModule,
		MatTooltipModule,
		MatTableModule,
		MatPaginatorModule,
		MatSelectModule,
		MatMenuModule,
		MatChipsModule,
		MatRippleModule,
		MatDividerModule,
		MatBadgeModule,
		MatSortModule,
		SkeletonModule,
		MatDialogModule,
		MatRadioModule,
	],
})
export class MaterialModule {}
