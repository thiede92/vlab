import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ChartConfiguration, ChartData, ChartEvent, ChartType } from "chart.js";
import { BaseChartDirective } from "ng2-charts";

@Component({
	selector: "app-chart",
	templateUrl: "./chart.component.html",
	styleUrls: ["./chart.component.scss"],
})
export class ChartComponent implements OnInit {
	@ViewChild(BaseChartDirective) chart: BaseChartDirective | undefined;
	@Input() type: ChartType = "bar";
	@Input() labels: any[] = ["2006", "2007", "2008", "2009", "2010", "2011", "2012"];
	@Input() datasets: any[] = [];

	ngOnChanges(changes: any) {
		console.log(changes);
		this.barChartData.labels = [];
		this.barChartData.datasets[0].data = [];
		this.barChartType = this.type;
		this.labels.forEach(item => {
			this.barChartData.labels?.push(item);
		});
		this.datasets.forEach(item => {
			this.barChartData.datasets[0].data.push(item);
		});
		this.barChartData = { ...this.barChartData };
	}

	ngOnInit() {
		this.barChartType = this.type;
	}

	public barChartOptions: ChartConfiguration["options"] = {
		responsive: true,
		// We use these empty structures as placeholders for dynamic theming.
		scales: {
			x: {},
			y: {
				min: 10,
			},
		},
		plugins: {
			legend: {
				display: true,
			},
		},
	};
	public barChartType: ChartType = "line";

	public barChartData: ChartData<"bar"> = {
		labels: ["2006", "2007", "2008", "2009", "2010", "2011", "2012"],
		datasets: [
			{ data: [65, 59, 80, 81, 56, 55, 40], label: "Series A", backgroundColor: "#3f51b5" },
			// { data: [ 28, 48, 40, 19, 86, 27, 90 ], label: 'Series B' }
		],
	};

	// events
	public chartClicked({ event, active }: { event?: ChartEvent; active?: {}[] }): void {
		console.log(event, active);
	}

	public chartHovered({ event, active }: { event?: ChartEvent; active?: {}[] }): void {
		console.log(event, active);
	}

	public randomize(): void {
		// Only Change 3 values
		this.barChartData.datasets[0].data = [
			Math.round(Math.random() * 100),
			59,
			80,
			Math.round(Math.random() * 100),
			56,
			Math.round(Math.random() * 100),
			40,
		];

		this.chart?.update();
	}
}
