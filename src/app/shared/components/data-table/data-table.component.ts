import {
	ChangeDetectionStrategy,
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewChild,
} from "@angular/core";
import { MatPaginator, MatPaginatorIntl } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { TranslateService } from "@ngx-translate/core";
import { IColumnConfig } from "src/app/core/models/data-table";
import { HttpStudentsService } from "../../../modules/students/service/http-students/http-students.service";
import { Subscription } from "rxjs";
import {
	MatSnackBar,
	MatSnackBarHorizontalPosition,
	MatSnackBarVerticalPosition,
} from "@angular/material/snack-bar";
import { StudentAddEditModalComponent } from "../../../modules/students/components/student-add-modal/student-add-edit-modal.component";
import { MatDialog } from "@angular/material/dialog";

@Component({
	selector: "app-data-table",
	templateUrl: "./data-table.component.html",
	styleUrls: ["./data-table.component.scss"],
})
export class DataTableComponent implements OnInit {
	@ViewChild(MatPaginator) paginator!: MatPaginator;
	@ViewChild(MatSort) sort!: MatSort;
	@ViewChild(MatPaginatorIntl) paginatorIntl!: MatPaginatorIntl;

	@Input() columnConfig: Array<IColumnConfig> = [];
	@Input() data: Array<any> | undefined;
	@Input() title: string = "";

	@Output() onEdit = new EventEmitter();

	horizontalPosition: MatSnackBarHorizontalPosition = "end";
	verticalPosition: MatSnackBarVerticalPosition = "bottom";

	icon = "icon";
	actions = "actions";
	activity = "isActive";
	student!: Subscription;
	displayedColumns: any = [];
	dataSource!: MatTableDataSource<any>;

	isDataUndefined = true;

	constructor(
		private readonly translateService: TranslateService,
		private studentsService: HttpStudentsService,
		private snackBar: MatSnackBar,
		private translate: TranslateService,
		private readonly dialog: MatDialog,
	) {
		this.translateService.onLangChange.subscribe({
			next: () => {
				if (this.paginator) {
					this.translatePaginatorLabels();
					this.dataSource.paginator = this.paginator;
				}
			},
		});
	}

	ngOnInit(): void {
		this.columnConfig.forEach((item: any) => {
			this.displayedColumns.push(item.name);
		});
	}

	async ngOnChanges(changes: any): Promise<void> {
		if (changes.data.currentValue !== undefined) {
			await this.declareTableData(this.data);
			this.dataSource.paginator = this.paginator;
			this.dataSource.sort = this.sort;
			this.translatePaginatorLabels();
		}
	}

	declareTableData(data: any): Promise<any> {
		return new Promise<void>(resolve => {
			this.dataSource = new MatTableDataSource<any>(data);
			this.isDataUndefined = false;
			resolve();
		});
	}

	translatePaginatorLabels() {
		this.paginator._intl.firstPageLabel = this.translateService.instant("table.first-page");
		this.paginator._intl.lastPageLabel = this.translateService.instant("table.last-page");
		this.paginator._intl.previousPageLabel = this.translateService.instant("table.previous-page");
		this.paginator._intl.nextPageLabel = this.translateService.instant("table.next-page");
		this.paginator._intl.itemsPerPageLabel =
			this.translateService.instant("table.records-per-page");
		const originalGetRangeLabel = this.paginator._intl.getRangeLabel;
		this.paginator._intl.getRangeLabel = (page: number, size: number, len: number) => {
			return originalGetRangeLabel(page, size, len).replace(
				"of",
				this.translateService.currentLang === "en" ? "of" : "z",
			);
		};
	}

	createAddEditStudentModal(idOfStudent?: any) {
		this.dialog
			.open(StudentAddEditModalComponent, {
				data: {
					title: this.translateService.instant("common.edit"),
					studentId: idOfStudent,
				},
				width: "500px",
				panelClass: "custom-class",
				autoFocus: true,
				restoreFocus: true,
				disableClose: true,
			})
			.afterClosed()
			.subscribe({
				next: () => this.onEdit.emit(true),
			});
	}

	announceDataChanges() {
		this.dataSource.paginator = this.paginator;
		this.dataSource.sort = this.sort;
		this.dataSource._updateChangeSubscription();
	}

	openSnackBar(message: string, action: string = this.translate.instant("common.close")) {
		this.snackBar.open(message, action, {
			duration: 4000,
			horizontalPosition: this.horizontalPosition,
			verticalPosition: this.verticalPosition,
		});
	}

	applyFilter(event: Event) {
		const filterValue = (event.target as HTMLInputElement).value;
		this.dataSource.filter = filterValue.trim().toLowerCase();
	}
}
