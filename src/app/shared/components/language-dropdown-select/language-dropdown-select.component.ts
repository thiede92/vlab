import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-language-dropdown-select",
	templateUrl: "./language-dropdown-select.component.html",
	styleUrls: ["./language-dropdown-select.component.scss"],
})
export class LanguageDropdownSelectComponent implements OnInit {
	currentLanguage: string = this.getCurrentLanguageClass();

	constructor(private readonly translateService: TranslateService) {
		const lang = window.localStorage.getItem("lang");
		if (lang) {
			this.currentLanguage = lang;
		}
	}

	ngOnInit(): void {}

	getCurrentLanguageClass(): string {
		return this.translateService.getDefaultLang();
	}

	setCurrentLanguage(lang: string): void {
		this.translateService.use(lang);
		this.currentLanguage = lang;
		window.localStorage.setItem("lang", lang);
	}
}
