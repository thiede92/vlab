import { ComponentFixture, TestBed } from "@angular/core/testing";

import { LanguageDropdownSelectComponent } from "./language-dropdown-select.component";

describe("LanguageDropdownSelectComponent", () => {
	let component: LanguageDropdownSelectComponent;
	let fixture: ComponentFixture<LanguageDropdownSelectComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [LanguageDropdownSelectComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(LanguageDropdownSelectComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
