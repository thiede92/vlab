import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormBuilder } from "@angular/forms";

@Component({
	selector: "app-icon-change-dropdown",
	templateUrl: "./icon-change-dropdown.component.html",
	styleUrls: ["./icon-change-dropdown.component.scss"],
})
export class IconChangeDropdownComponent implements OnInit {
	@Output() iconChanged: EventEmitter<any> = new EventEmitter;
	
	iconTypes = [
		{
			name: "science",
		},
		{
			name: "rocket_launch",
		},
		{
			name: "biotech",
		},
		{
			name: "groups",
		},
		{
			name: "psychology_alt",
		},
		{
			name: "psychology",
		},
		{
			name: "cast_for_education",
		},
		{
			name: "workspace_premium",
		},
	];
	iconColors = [
		{
			name: "white",
		},
		{
			name: "darkblue",
		},
		{
			name: "#3f51b5",
		},
		{
			name: "dodgerblue",
		},
		{
			name: "green",
		},
		{
			name: "darkcyan",
		},
		{
			name: "red",
		},
		{
			name: "#ff4081",
		},
		{
			name: "orange",
		},
	];
	iconBackgrounds = [
		{
			name: "darkblue",
		},
		{
			name: "#3f51b5",
		},
		{
			name: "dodgerblue",
		},
		{
			name: "green",
		},
		{
			name: "darkcyan",
		},
		{
			name: "red",
		},
		{
			name: "#ff4081",
		},
		{
			name: "orange",
		},
	];

	configIconForm = this.fb.group({
		"icon-type": ["rocket_launch"],
		"icon-color": ["white"],
		"icon-background": ["darkblue"],
	});

	constructor(private readonly fb: FormBuilder) {}

	ngOnInit(): void {
		this.emitInitialIconValue();
		this.configIconForm.valueChanges.subscribe({
			next: (data) => {
				this.iconChanged.emit(data);
			},
		})
	}

	emitInitialIconValue() {
		this.iconChanged.emit(this.configIconForm.value);
	}
}
