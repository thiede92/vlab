import { ComponentFixture, TestBed } from "@angular/core/testing";

import { IconChangeDropdownComponent } from "./icon-change-dropdown.component";

describe("IconChangeDropdownComponent", () => {
	let component: IconChangeDropdownComponent;
	let fixture: ComponentFixture<IconChangeDropdownComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [IconChangeDropdownComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(IconChangeDropdownComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
