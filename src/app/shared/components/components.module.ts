import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { LanguageDropdownSelectComponent } from "./language-dropdown-select/language-dropdown-select.component";
import { DataTableComponent } from "./data-table/data-table.component";
import { MaterialModule } from "../material/material.module";
import { ChartComponent } from "./chart/chart.component";
import { NgChartsModule } from "ng2-charts";
import { TranslateModule } from "@ngx-translate/core";
import { IconChangeDropdownComponent } from "./icon-change-dropdown/icon-change-dropdown.component";
import { SharedModule } from "../shared.module";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
	declarations: [
		LanguageDropdownSelectComponent,
		DataTableComponent,
		ChartComponent,
		IconChangeDropdownComponent,
	],
	imports: [CommonModule, MaterialModule, NgChartsModule, TranslateModule, ReactiveFormsModule],
	exports: [
		LanguageDropdownSelectComponent,
		DataTableComponent,
		ChartComponent,
		IconChangeDropdownComponent,
	],
})
export class ComponentsModule {}
