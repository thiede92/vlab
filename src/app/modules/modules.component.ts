import { Component, OnInit } from "@angular/core";
import { ChildrenOutletContexts } from "@angular/router";
import { slideInAnimation } from "../animations";

@Component({
	selector: "app-modules",
	templateUrl: "./modules.component.html",
	styleUrls: ["./modules.component.scss"],
	animations: [slideInAnimation],
})
export class ModulesComponent implements OnInit {
	loading = false;

	constructor(private contexts: ChildrenOutletContexts) {}

	ngOnInit(): void {}

	getRouteAnimationData() {
		return this.contexts.getContext("primary")?.route?.snapshot?.data?.["animation"];
	}
}
