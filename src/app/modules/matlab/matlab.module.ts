import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MatlabRoutingModule } from "./matlab-routing.module";
import { MatlabComponent } from "./matlab.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
	declarations: [MatlabComponent],
	imports: [CommonModule, MatlabRoutingModule, SharedModule],
})
export class MatlabModule {}
