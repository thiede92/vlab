import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ScenariosRoutingModule } from "./scenarios-routing.module";
import { ScenariosComponent } from "./scenarios.component";
import { SharedModule } from "src/app/shared/shared.module";
import { ScenarioCreateModalComponent } from './components/scenario-create-modal/scenario-create-modal.component';

@NgModule({
	declarations: [ScenariosComponent, ScenarioCreateModalComponent],
	imports: [CommonModule, ScenariosRoutingModule, SharedModule],
})
export class ScenariosModule {}
