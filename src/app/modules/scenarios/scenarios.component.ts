import { Component, OnInit } from "@angular/core";
import { MatDialog } from '@angular/material/dialog';
import { ScenarioCreateModalComponent } from './components/scenario-create-modal/scenario-create-modal.component';

@Component({
	selector: "app-scenarios",
	templateUrl: "./scenarios.component.html",
	styleUrls: ["./scenarios.component.scss"],
})
export class ScenariosComponent implements OnInit {
	constructor(private readonly dialog: MatDialog) {}

	ngOnInit(): void {}

	onShowCreateScenarioModal() {
		const ref = this.dialog.open(ScenarioCreateModalComponent, {
			data: {
				title: "common.filtering",
			},
			width: "500px",
			panelClass: "custom-class",
			autoFocus: false,
			restoreFocus: false,
			disableClose: true
		});
	}
}
