import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScenarioCreateModalComponent } from './scenario-create-modal.component';

describe('ScenarioCreateModalComponent', () => {
  let component: ScenarioCreateModalComponent;
  let fixture: ComponentFixture<ScenarioCreateModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScenarioCreateModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ScenarioCreateModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
