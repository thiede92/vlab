import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-scenario-create-modal',
  templateUrl: './scenario-create-modal.component.html',
  styleUrls: ['./scenario-create-modal.component.scss']
})
export class ScenarioCreateModalComponent implements OnInit {

  constructor(private readonly dialogRef: MatDialogRef<ScenarioCreateModalComponent>) { }

  ngOnInit(): void {
  }

	onCloseModal() {
		this.dialogRef.close();
	}
}
