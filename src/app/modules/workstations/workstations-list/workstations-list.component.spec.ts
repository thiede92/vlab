import { ComponentFixture, TestBed } from "@angular/core/testing";

import { WorkstationsListComponent } from "./workstations-list.component";

describe("WorkstationsListComponent", () => {
	let component: WorkstationsListComponent;
	let fixture: ComponentFixture<WorkstationsListComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [WorkstationsListComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(WorkstationsListComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
