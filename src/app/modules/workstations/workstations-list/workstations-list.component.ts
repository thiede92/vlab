import { Component, OnInit } from "@angular/core";
import { map } from "rxjs";
import { IColumnConfig } from "src/app/core/models/data-table";
import { IStudent } from "src/app/core/models/student";
import { HttpStudentsService } from "../../students/service/http-students/http-students.service";
import { WorkstationsCreateModalComponent } from "../workstations-create-modal/workstations-create-modal.component";
import { MatDialog } from "@angular/material/dialog";
import { HttpWorkstationsService } from "../services/http-workstations/http-workstations.service";
import { UntilDestroy, untilDestroyed } from "@ngneat/until-destroy";

@UntilDestroy()
@Component({
	selector: "app-workstations-list",
	templateUrl: "./workstations-list.component.html",
	styleUrls: ["./workstations-list.component.scss"],
})
export class WorkstationsListComponent implements OnInit {
	workstationsData = <any>[];
	workstationsTableTitle: string = "Lista stanowisk";
	workstationsColumnConfig: Array<any> = [
		{
			name: "icon",
			columnName: "common.icon",
		},
		{
			name: "name",
			columnName: "common.name",
		},
		{
			name: "totalMeasurments",
			columnName: "workstations.total-measurments",
		},
		// {
		// 	name: "description",
		// 	columnName: "common.description",
		// },
		{
			name: "createdAt",
			columnName: "common.created-at",
		},

		{
			name: "isActive",
			columnName: "common.is-active",
		},
	];

	studentsColumnConfig: Array<IColumnConfig> = [
		{
			name: "email",
			columnName: "common.email",
		},
		{
			name: "firstName",
			columnName: "common.first-name",
		},
		{
			name: "lastName",
			columnName: "common.last-name",
		},
		{
			name: "groups",
			columnName: "common.group",
		},
		{
			name: "address",
			columnName: "common.address",
		},
		{
			name: "postCode",
			columnName: "common.post-code",
		},
		{
			name: "city",
			columnName: "common.city",
		},
		{
			name: "albumNumber",
			columnName: "common.album-number",
		},
	];
	studentsData: Array<IStudent> | undefined = undefined;

	constructor(
		private readonly httpStudentsService: HttpStudentsService,
		private readonly htttpWorkstationsService: HttpWorkstationsService,
		private readonly dialog: MatDialog,
	) {
		setTimeout(() => {
			this.getStudents();
		}, 1000);
	}

	ngOnInit(): void {
		this.getAllWorkstations();
	}

	getAllWorkstations() {
		this.htttpWorkstationsService
			.getAll()
			.pipe(
				map((data: any) => {
					const arr: any = [];
					data.forEach((item: any) => {
						const createdAt = new Date(item.createdAt).toLocaleString("pl-PL");
						arr.push({ ...item, createdAt: createdAt });
					});
					return arr;
				}),
			)
			.subscribe({
				next: (data: any) => {
					this.workstationsData = [...data];
				},
			});
	}

	getStudents() {
		this.httpStudentsService
			.getAll()
			.pipe(
				map((data: any) => {
					return data.map((student: any) => {
						return {
							email: student.email,
							firstName: student.firstName,
							lastName: student.lastName,
							address: student.address,
							city: student.city,
							postCode: student.postCode,
							albumNumber: student.albumNumber,
							groups: "-",
						};
					});
				}, untilDestroyed(this)),
			)
			.subscribe({
				next: (data: any) => (this.studentsData = data),
			});
	}

	onShowCreateWorkstationModal() {
		const ref = this.dialog.open(WorkstationsCreateModalComponent, {
			data: {
				title: "workstations.create-workstation",
			},
			width: "500px",
			panelClass: "custom-class",
			autoFocus: false,
			restoreFocus: false,
			disableClose: true,
		});
	}
}
