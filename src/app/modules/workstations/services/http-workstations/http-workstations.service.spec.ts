import { TestBed } from "@angular/core/testing";

import { HttpWorkstationsService } from "./http-workstations.service";

describe("HttpWorskationsService", () => {
	let service: HttpWorkstationsService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(HttpWorkstationsService);
	});

	it("should be created", () => {
		expect(service).toBeTruthy();
	});
});
