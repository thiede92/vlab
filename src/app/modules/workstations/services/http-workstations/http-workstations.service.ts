import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { fakeApiUrl } from "src/app/core/api-config";
import { UntilDestroy, untilDestroyed } from "@ngneat/until-destroy";

@UntilDestroy()
@Injectable({
	providedIn: "root",
})
export class HttpWorkstationsService {
	url = fakeApiUrl;

	constructor(private readonly http: HttpClient) {}

	getAll() {
		return this.http.get(`${this.url}/workstations`).pipe(untilDestroyed(this));
	}

	create(workstation: any) {
		return this.http.post(`${this.url}/workstations`, workstation).pipe(untilDestroyed(this));
	}
}
