import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { WorkstationsRoutingModule } from "./workstations-routing.module";
import { WorkstationsComponent } from "./workstations.component";
import { SharedModule } from "src/app/shared/shared.module";
import { WorkstationsListComponent } from "./workstations-list/workstations-list.component";
import { WorkstationsCreateModalComponent } from "./workstations-create-modal/workstations-create-modal.component";

@NgModule({
	declarations: [
		WorkstationsComponent,
		WorkstationsListComponent,
		WorkstationsCreateModalComponent,
	],
	imports: [CommonModule, WorkstationsRoutingModule, SharedModule],
})
export class WorkstationsModule {}
