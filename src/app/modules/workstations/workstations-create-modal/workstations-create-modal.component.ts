import { Component, Inject, OnInit } from "@angular/core";
import { FormBuilder, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { HttpWorkstationsService } from "../services/http-workstations/http-workstations.service";

@Component({
	selector: "app-workstations-create-modal",
	templateUrl: "./workstations-create-modal.component.html",
	styleUrls: ["./workstations-create-modal.component.scss"],
})
export class WorkstationsCreateModalComponent implements OnInit {
	title: string = ``;
	createWorkstationForm = this.fb.group({
		name: ["", [Validators.required]],
		description: [""],
		apiUrl: [""],
		icon: ["", Validators.required],
		// "results-presentation-type": ["", Validators.required],
		// "chart-type": ["line"],
		// "mock-data-labels": ["", Validators.required],
		// "mock-data-datasets": ["", Validators.required],
	});

	workstationDataForm = this.fb.group({
		label: [""],
		dataset: [""],
	});

	labels: any[] = [0, 1.6, 3.2, 4.8, 6.4, 8, 9.6];
	datasets: any[] = [30, 36, 47, 68, 88, 109, 92];

	constructor(
		@Inject(MAT_DIALOG_DATA) public data: any,
		private readonly dialogRef: MatDialogRef<WorkstationsCreateModalComponent>,
		private readonly fb: FormBuilder,
		private readonly workstationHttp: HttpWorkstationsService,
	) {}

	ngOnInit(): void {
		this.title = this.data.title;
	}

	onCloseModal() {
		this.dialogRef.close();
	}

	getErrorMessage(controlName: string) {
		const errorType: any = this.createWorkstationForm.get(controlName)?.errors;
		for (let [key, value] of Object.entries(errorType)) {
			if (key === "required") {
				return "error.this-field-is-required";
			} else if (key === "pattern" && controlName === "email") {
				return "error.provided-email-has-an-incorrect-format";
			}
		}
		return "";
	}

	addLabel(value: any): void {
		this.labels = [...this.labels, value];
	}

	removeLabel(label: any): void {
		const index = this.labels.indexOf(label);
		if (index >= 0) {
			this.labels.splice(index, 1);
		}
		this.labels = [...this.labels];
	}

	addDataset(value: any): void {
		this.datasets = [...this.datasets, value];
	}

	removeDataset(dataset: any): void {
		const index = this.datasets.indexOf(dataset);
		if (index >= 0) {
			this.datasets.splice(index, 1);
		}
		this.datasets = [...this.datasets];
	}

	createWorkstation(workstationData: any) {
		const currentDate = Date.now();
		workstationData = {
			...workstationData,
			totalMeasurments: 0,
			createdAt: currentDate,
			isActive: true,
		};
		this.workstationHttp.create(workstationData).subscribe({
			next: response => console.log(response),
		});
		this.dialogRef.close();
		console.log(`new workstation:`, workstationData);
	}

	iconChanged(event: any) {
		this.createWorkstationForm.get("icon")?.setValue(event);
	}
}
