import { ComponentFixture, TestBed } from "@angular/core/testing";

import { WorkstationsCreateModalComponent } from "./workstations-create-modal.component";

describe("WorkstationsCreateModalComponent", () => {
	let component: WorkstationsCreateModalComponent;
	let fixture: ComponentFixture<WorkstationsCreateModalComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [WorkstationsCreateModalComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(WorkstationsCreateModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
