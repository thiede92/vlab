import { TestBed } from "@angular/core/testing";

import { MeasurementLogsService } from "./measurement-logs.service";

describe("MeasurementLogsService", () => {
	let service: MeasurementLogsService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(MeasurementLogsService);
	});

	it("should be created", () => {
		expect(service).toBeTruthy();
	});
});
