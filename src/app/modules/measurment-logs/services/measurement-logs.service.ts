import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { IMeasurmentLog } from "../../../core/models/measurment-logs";
import { url } from "../../../core/api-config";
import { UntilDestroy, untilDestroyed } from "@ngneat/until-destroy";

@UntilDestroy()
@Injectable({
	providedIn: "root",
})
export class MeasurementLogsService {
	constructor(private readonly http: HttpClient) {}

	getAll() {
		return this.http.get<IMeasurmentLog[]>(`${url}/logs`).pipe(untilDestroyed(this));
	}

	getLog(id: string) {
		return this.http.get<IMeasurmentLog>(`${url}/logs`).pipe(untilDestroyed(this));
	}

	create(data: IMeasurmentLog) {
		return this.http.post<IMeasurmentLog>(`${url}/logs`, { ...data }).pipe(untilDestroyed(this));
	}

	update(data: IMeasurmentLog, id: string) {
		return this.http
			.patch<IMeasurmentLog>(`${url}/logs/${id}`, { ...data })
			.pipe(untilDestroyed(this));
	}
}
