import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { MeasurmentLogsRoutingModule } from "./measurment-logs-routing.module";
import { MeasurmentLogsComponent } from "./measurment-logs.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
	declarations: [MeasurmentLogsComponent],
	imports: [CommonModule, MeasurmentLogsRoutingModule, SharedModule],
})
export class MeasurmentLogsModule {}
