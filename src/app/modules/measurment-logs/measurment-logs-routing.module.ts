import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MeasurmentLogsComponent } from "./measurment-logs.component";

const routes: Routes = [{ path: "", component: MeasurmentLogsComponent }];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class MeasurmentLogsRoutingModule {}
