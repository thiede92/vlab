import { ComponentFixture, TestBed } from "@angular/core/testing";

import { MeasurmentLogsComponent } from "./measurment-logs.component";

describe("MeasurmentLogsComponent", () => {
	let component: MeasurmentLogsComponent;
	let fixture: ComponentFixture<MeasurmentLogsComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [MeasurmentLogsComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(MeasurmentLogsComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
