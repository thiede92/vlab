import { Component, OnInit, ViewChild } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthService } from "src/app/core/services/auth/auth.service";
import { IColumnConfig } from "src/app/core/models/data-table";

@Component({
	selector: "app-dashboard",
	templateUrl: "./dashboard.component.html",
	styleUrls: ["./dashboard.component.scss"],
})
export class DashboardComponent implements OnInit {
	tableColumnConfig: Array<IColumnConfig> = [];
	tableData: any = [];
	tableTitle: string = "";

	constructor(private readonly http: HttpClient, private readonly authService: AuthService) {
		this.tableColumnConfig = [
			{
				name: "name",
				columnName: "Name",
			},
			{
				name: "lastName",
				columnName: "Last name",
			},
			{
				name: "email",
				columnName: "Email",
			},
			{
				name: "group",
				columnName: "Group",
			},
			{
				name: "field",
				columnName: "Field",
			},
			{
				name: "city",
				columnName: "City",
			},
			{
				name: "year",
				columnName: "Year",
			},
		];
		for (let i = 0; i < 24; i++) {
			this.tableData.push({
				name: "John",
				lastName: "Doe",
				email: "john.doe@wsg.pl",
				field: "Informatics",
				city: "Bydgoszcz",
				group: "3 N122L-PiTwww",
				year: "3",
			});
		}
		setTimeout(() => {
			for (let i = 0; i < 6; i++) {
				this.tableData.push({
					name: "Johnny",
					lastName: "Doe",
					email: "john.doe@wsg.pl",
					field: "Informatics",
					city: "Bydgoszcz",
					group: "3 N122L-PiTwww",
					year: "3",
				});
			}
			this.tableData = [...this.tableData];
		}, 3000);
		this.tableTitle = "users-list";
	}

	ngOnInit(): void {}
}
