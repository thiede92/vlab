import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
	providedIn: "root",
})
export class HttpDashboardService {
	constructor(private readonly http: HttpClient) {}

	//... tbd.
	// w przyszłości będzie tutaj kilkanaście różnych funkcji
	// które zaciągają dane z innych serwisów i nie tylko.
}
