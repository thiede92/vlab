import { TestBed } from "@angular/core/testing";

import { HttpDashboardService } from "./http-dashboard.service";

describe("HttpDashboardService", () => {
	let service: HttpDashboardService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(HttpDashboardService);
	});

	it("should be created", () => {
		expect(service).toBeTruthy();
	});
});
