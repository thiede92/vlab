import { Component, OnInit } from "@angular/core";
import { map, Observable, of } from "rxjs";
import { IColumnConfig } from "src/app/core/models/data-table";
import { IStudent } from "src/app/core/models/student";
import { HttpStudentsService } from "./service/http-students/http-students.service";
import { MatDialog } from "@angular/material/dialog";
import { StudentAddEditModalComponent } from "./components/student-add-modal/student-add-edit-modal.component";
import { CsvDataService } from "../../core/services/csv/csv.service";
import { StudentUploadModalComponent } from "./components/student-upload-modal/student-upload-modal.component";
import { UntilDestroy, untilDestroyed } from "@ngneat/until-destroy";
import { ToastrService } from "ngx-toastr";
import { TranslateService } from "@ngx-translate/core";

@UntilDestroy()
@Component({
	selector: "app-students",
	templateUrl: "./students.component.html",
	styleUrls: ["./students.component.scss"],
})
export class StudentsComponent implements OnInit {
	studentsColumnConfig: Array<IColumnConfig> = [
		{
			name: "albumNumber",
			columnName: "common.album-number",
		},
		{
			name: "lastName",
			columnName: "common.last-name",
		},
		{
			name: "firstName",
			columnName: "common.first-name",
		},
		{
			name: "groups",
			columnName: "common.group",
		},
		{
			name: "email",
			columnName: "common.email",
		},
		{
			name: "address",
			columnName: "common.address",
		},
		{
			name: "city",
			columnName: "common.city",
		},
		{
			name: "postCode",
			columnName: "common.post-code",
		},
		{
			name: "isActive",
			columnName: "common.active",
		},
		{
			name: "actions",
			columnName: "",
		},
	];
	studentsData: Array<IStudent> | undefined;
	studentsData$!: Observable<Array<IStudent> | undefined>;
	studentsTableTitle: string = "table.students-list";

	constructor(
		private readonly httpStudentsService: HttpStudentsService,
		private readonly dialog: MatDialog,
		private csvService: CsvDataService,
		private toastr: ToastrService,
		private translateService: TranslateService,
	) {
		setTimeout(() => {
			this.getStudents();
		}, 0);
	}

	ngOnInit(): void {
		this.studentsData$ = of(this.studentsData);
	}

	getStudents() {
		this.httpStudentsService
			.getAll()
			.pipe(
				map((data: any) => {
					return data.map((student: any) => {
						return {
							id: student.id,
							email: student.email,
							firstName: student.firstName,
							lastName: student.lastName,
							address: student.address,
							city: student.city,
							postCode: student.postCode,
							albumNumber: student.albumNumber,
							groups: student.groups,
							isActive: student.isActive,
						};
					});
				}, untilDestroyed(this)),
			)
			.subscribe({
				next: data => (this.studentsData = data),
				error: err => {
					const error = this.translateService.instant(err);

					this.toastr.error(error);
				},
			});
	}

	convertStudentsDataToCSV() {
		this.csvService.SaveAsCSV(this.studentsData);
	}

	createAddNewStudentModal() {
		this.dialog
			.open(StudentAddEditModalComponent, {
				data: {
					title: "",
				},
				width: "500px",
				panelClass: "custom-class",
				autoFocus: false,
				restoreFocus: false,
				disableClose: true,
			})
			.afterClosed()
			.subscribe({
				next: () => this.getStudents(),
				error: e => {
					const error = this.translateService.instant(e);

					this.toastr.error(error);
				},
			});
	}

	createUploadingCSVModal() {
		this.dialog
			.open(StudentUploadModalComponent, {
				data: {
					title: "",
				},
				width: "500px",
				panelClass: "custom-class",
				autoFocus: false,
				restoreFocus: false,
				disableClose: false,
			})
			.afterClosed()
			.subscribe({
				next: () => this.getStudents(),
				error: e => {
					const error = this.translateService.instant(e);

					this.toastr.error(error);
				},
			});
	}
}
