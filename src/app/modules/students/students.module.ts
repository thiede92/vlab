import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { StudentsRoutingModule } from "./students-routing.module";
import { StudentsComponent } from "./students.component";
import { SharedModule } from "src/app/shared/shared.module";
import { StudentsDetailComponent } from "./students-detail/students-detail.component";
import { StudentFilteringModalComponent } from "./components/student-filtering-modal/student-filtering-modal.component";
import { StudentAddEditModalComponent } from "./components/student-add-modal/student-add-edit-modal.component";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { StudentUploadModalComponent } from "./components/student-upload-modal/student-upload-modal.component";
import { MatAutocompleteModule } from "@angular/material/autocomplete";

@NgModule({
	declarations: [
		StudentsComponent,
		StudentsDetailComponent,
		StudentFilteringModalComponent,
		StudentAddEditModalComponent,
		StudentUploadModalComponent,
	],
	imports: [
		CommonModule,
		StudentsRoutingModule,
		SharedModule,
		MatSlideToggleModule,
		MatAutocompleteModule,
	],
})
export class StudentsModule {}
