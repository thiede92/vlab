import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { url } from "src/app/core/api-config";
import { IStudent } from "src/app/core/models/student";
import { UntilDestroy } from "@ngneat/until-destroy";
import { ErrorHandlerService } from "../../../../core/services/error/error-handler.service";
import { catchError } from "rxjs";

@UntilDestroy()
@Injectable({
	providedIn: "root",
})
export class HttpStudentsService {
	constructor(private readonly http: HttpClient, private errorHandler: ErrorHandlerService) {}

	getStudent(id: string) {
		return this.http
			.get<IStudent>(`${url}/users/${id}`)
			.pipe(catchError(this.errorHandler.handleError));
	}

	getAll() {
		return this.http
			.get(`${url}/users?role=student`)
			.pipe(catchError(this.errorHandler.handleError));
	}

	create(student: IStudent) {
		return this.http
			.post(`${url}/users`, { ...student, role: ["student"] })
			.pipe(catchError(this.errorHandler.handleError));
	}

	update(student: IStudent, id: string) {
		return this.http
			.patch(`${url}/users/${id}`, { ...student })
			.pipe(catchError(this.errorHandler.handleError));
	}
	sendData(data: FormData) {
		return this.http
			.post(`${url}/studentsData`, { ...data })
			.pipe(catchError(this.errorHandler.handleError));
	}
}
