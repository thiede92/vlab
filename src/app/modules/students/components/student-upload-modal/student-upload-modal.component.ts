import { Component, ElementRef, HostListener, OnInit, ViewChild } from "@angular/core";
import { HttpStudentsService } from "../../service/http-students/http-students.service";
import { TranslateService } from "@ngx-translate/core";
import { untilDestroyed } from "@ngneat/until-destroy";

@Component({
	selector: "app-student-upload-modal",
	templateUrl: "./student-upload-modal.component.html",
	styleUrls: ["./student-upload-modal.component.scss"],
})
export class StudentUploadModalComponent implements OnInit {
	file!: any;
	currentProgress: string = this.translate.instant("common.sending");
	@ViewChild("fileUploadProgress") progressElement!: ElementRef<HTMLDivElement>;

	@HostListener("dragover", ["$event"]) onDragOver(event: any) {
		event.preventDefault();
		event.stopPropagation();
	}

	@HostListener("dragleave", ["$event"]) onDragLeave(event: any) {
		event.preventDefault();
		event.stopPropagation();
	}

	@HostListener("drop", ["$event"])
	public ondrop(event: any) {
		event.preventDefault();
		event.stopPropagation();
		let fileData = event.dataTransfer.files[0];
		this.file = fileData.name;
	}

	constructor(private httpService: HttpStudentsService, private translate: TranslateService) {}

	ngOnInit(): void {}

	// Runs once we pass data
	getFile(event: any) {
		this.file = event.target.files[0].name;

		this.uploadFile().then(data => {
			this.httpService
				.sendData(data)
				.pipe(untilDestroyed(this))
				.subscribe({
					next: () => {
						this.progressElement.nativeElement.style.setProperty(
							"--vlab-progress-bar-width",
							"50%",
						);
					},
					error: () => {
						this.progressElement.nativeElement.style.setProperty(
							"--vlab-progress-bar-width",
							"100%",
						);
						this.currentProgress = this.translate.instant("students.adding-by-csv-file-successful");
					},
				});
		});
	}

	// Runs when we get 100% of information about a file
	async uploadFile(): Promise<FormData> {
		let formData = new FormData();
		formData.set("new-students-data-file", this.file);
		return formData;
	}
}
