import { ComponentFixture, TestBed } from "@angular/core/testing";

import { StudentUploadModalComponent } from "./student-upload-modal.component";

describe("CsvUploadModalComponent", () => {
	let component: StudentUploadModalComponent;
	let fixture: ComponentFixture<StudentUploadModalComponent>;

	beforeEach(async () => {
		await TestBed.configureTestingModule({
			declarations: [StudentUploadModalComponent],
		}).compileComponents();
	});

	beforeEach(() => {
		fixture = TestBed.createComponent(StudentUploadModalComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it("should create", () => {
		expect(component).toBeTruthy();
	});
});
