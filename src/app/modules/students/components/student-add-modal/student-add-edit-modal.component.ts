import { Component, Inject, OnInit } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { StudentFilteringModalComponent } from "../student-filtering-modal/student-filtering-modal.component";
import { HttpStudentsService } from "../../service/http-students/http-students.service";
import {
	MatSnackBar,
	MatSnackBarHorizontalPosition,
	MatSnackBarVerticalPosition,
} from "@angular/material/snack-bar";
import { TranslateService } from "@ngx-translate/core";
import { map, Observable, startWith } from "rxjs";
import { UntilDestroy, untilDestroyed } from "@ngneat/until-destroy";
import { ToastrService } from "ngx-toastr";

@UntilDestroy()
@Component({
	selector: "app-student-add-modal",
	templateUrl: "./student-add-edit-modal.component.html",
	styleUrls: ["./student-add-edit-modal.component.scss"],
})
export class StudentAddEditModalComponent implements OnInit {
	horizontalPosition: MatSnackBarHorizontalPosition = "end";
	verticalPosition: MatSnackBarVerticalPosition = "bottom";
	listOfGroups: string[] = ["3N122Z-TWITPIC", "1N128Z-PU", "2N123L-UI"];
	//dev: te dane póki co są zamockowane, dopiero gdy będziemy mieli mechanizm tworzenia grup, trzeba będzie fetchować te dane w tym miejscu
	filteredGroups!: Observable<string[]> | undefined;

	addStudentForm = new FormGroup({
		email: new FormControl("", Validators.compose([Validators.required, Validators.email])),
		firstName: new FormControl(
			"",
			Validators.compose([Validators.pattern(/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s]+$/)]),
		),
		lastName: new FormControl(
			"",
			Validators.compose([Validators.pattern(/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s]+$/)]),
		),
		groups: new FormControl([], Validators.compose([Validators.pattern(/^\d\w\d\d\d\w\-\w+/)])),
		address: new FormControl("", Validators.compose([Validators.pattern(/\w+/)])),
		postCode: new FormControl("", Validators.compose([Validators.pattern(/\d\d\-\d\d\d/)])),
		city: new FormControl(
			"",
			Validators.compose([Validators.pattern(/^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ\s]+$/)]),
		),
		albumNumber: new FormControl("", Validators.compose([Validators.pattern(/\d\d\d\d\d\d/)])),
	});

	constructor(
		@Inject(MAT_DIALOG_DATA) public studentsData: any,
		private readonly dialogRef: MatDialogRef<StudentFilteringModalComponent>,
		private readonly httpService: HttpStudentsService,
		private readonly toastr: ToastrService,
		private snackBar: MatSnackBar,
		private translate: TranslateService,
	) {}

	private get groups() {
		return this.addStudentForm.get("groups");
	}

	ngOnInit() {
		setTimeout(() => {
			this.filterOptions();
		}, 1000);
		this.tryGetStudentsData();
	}

	onSubmit(): void {
		this.studentsData.studentId
			? this.updateStudent(this.addStudentForm.value)
			: this.addStudent(this.addStudentForm.value);
	}

	onCloseModal(): void {
		this.dialogRef.close();
	}

	addStudent(data: any) {
		const currentDate = Date.now();
		data = { ...data, isActive: true, Added: currentDate };
		this.httpService
			.create(data)
			.pipe(untilDestroyed(this))
			.subscribe({
				complete: () => {
					this.studentsData.studentId
						? this.openSnackBar(this.translate.instant("students.editing-student-success"))
						: this.openSnackBar(this.translate.instant("students.adding-new-one-success"));
					this.dialogRef.close();
				},
			});
	}

	updateStudent(data: any) {
		const currentDate = Date.now();
		this.httpService
			.update({ ...data, Added: currentDate }, this.studentsData.studentId)
			.pipe(untilDestroyed(this))
			.subscribe({
				complete: () => {
					this.studentsData.studentId
						? this.openSnackBar(this.translate.instant("students.editing-student-success"))
						: this.openSnackBar(this.translate.instant("students.adding-new-one-success"));
					this.dialogRef.close();
				},
			});
	}

	openSnackBar(message: string, action: string = this.translate.instant("common.close")) {
		this.snackBar.open(message, action, {
			duration: 4000,
			horizontalPosition: this.horizontalPosition,
			verticalPosition: this.verticalPosition,
		});
	}

	tryGetStudentsData() {
		if (!this.studentsData.studentId) return;

		this.httpService
			.getStudent(this.studentsData.studentId)
			.pipe(untilDestroyed(this))
			.subscribe({
				next: val => {
					this.addStudentForm.patchValue({ ...val });
				},
				error: err => {
					this.toastr.error(err);
				},
			});
	}

	private filterOptions() {
		this.filteredGroups = this.groups?.valueChanges.pipe(
			startWith(""),
			map(value => this._filter(value || "")),
		);
	}

	private _filter(value: string): string[] {
		const filterValue = value?.toLowerCase();
		return this.listOfGroups.filter(option => option.toLowerCase().includes(filterValue));
	}
}
