import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentAddEditModalComponent } from './student-add-edit-modal.component';

describe('StudentAddModalComponent', () => {
  let component: StudentAddEditModalComponent;
  let fixture: ComponentFixture<StudentAddEditModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentAddEditModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentAddEditModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
