import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentFilteringModalComponent } from './student-filtering-modal.component';

describe('StudentFilteringModalComponent', () => {
  let component: StudentFilteringModalComponent;
  let fixture: ComponentFixture<StudentFilteringModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StudentFilteringModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentFilteringModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
