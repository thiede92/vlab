import { Component } from "@angular/core";
import { MatDialogRef } from "@angular/material/dialog";
import { ThemePalette } from "@angular/material/core";

@Component({
	selector: "app-student-filtering-modal",
	templateUrl: "./student-filtering-modal.component.html",
	styleUrls: ["./student-filtering-modal.component.scss"],
})
export class StudentFilteringModalComponent {
	color: ThemePalette = "primary";
	checked = false;
	disabled = false;

	constructor(private readonly dialogRef: MatDialogRef<StudentFilteringModalComponent>) {}

	onCloseModal() {
		this.dialogRef.close();
	}
}
