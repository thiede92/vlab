import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AppGuard } from "../core/guards/app-guard/app.guard";
import { ModulesComponent } from "./modules.component";

const routes: Routes = [
	{
		path: "",
		component: ModulesComponent,
		children: [
			{
				path: "",
				redirectTo: "dashboard",
				pathMatch: "prefix",
			},
			{
				path: "dashboard",
				canActivate: [AppGuard],
				loadChildren: () => import("./dashboard/dashboard.module").then(m => m.DashboardModule),
			},
			{
				path: "schedules",
				canActivate: [AppGuard],
				loadChildren: () => import("./schedules/schedules.module").then(m => m.SchedulesModule),
			},
			{
				path: "groups",
				canActivate: [AppGuard],
				loadChildren: () => import("./groups/groups.module").then(m => m.GroupsModule),
			},
			{
				path: "students",
				canActivate: [AppGuard],
				loadChildren: () => import("./students/students.module").then(m => m.StudentsModule),
			},
			{
				path: "teachers",
				canActivate: [AppGuard],
				loadChildren: () => import("./teachers/teachers.module").then(m => m.TeachersModule),
			},
			{
				path: "workstations",
				canActivate: [AppGuard],
				loadChildren: () =>
					import("./workstations/workstations.module").then(m => m.WorkstationsModule),
			},
			{
				path: "scenarios",
				canActivate: [AppGuard],
				loadChildren: () => import("./scenarios/scenarios.module").then(m => m.ScenariosModule),
			},
			{
				path: "measurment-logs",
				canActivate: [AppGuard],
				loadChildren: () =>
					import("./measurment-logs/measurment-logs.module").then(m => m.MeasurmentLogsModule),
			},
			{
				path: "reports",
				canActivate: [AppGuard],
				loadChildren: () => import("./reports/reports.module").then(m => m.ReportsModule),
			},
			{
				path: "grades",
				canActivate: [AppGuard],
				loadChildren: () => import("./grades/grades.module").then(m => m.GradesModule),
			},
			{
				path: "matlab",
				canActivate: [AppGuard],
				loadChildren: () => import("./matlab/matlab.module").then(m => m.MatlabModule),
			},
		],
	},
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class ModulesRoutingModule {}
