import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { GradesRoutingModule } from "./grades-routing.module";
import { GradesComponent } from "./grades.component";
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
	declarations: [GradesComponent],
	imports: [CommonModule, GradesRoutingModule, SharedModule],
})
export class GradesModule {}
