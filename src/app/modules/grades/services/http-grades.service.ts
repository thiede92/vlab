import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { url } from "../../../core/api-config";
import { UntilDestroy, untilDestroyed } from "@ngneat/until-destroy";
import { IGrade } from "../../../core/models/grade";

@UntilDestroy()
@Injectable({
	providedIn: "root",
})
export class HttpGradesService {
	constructor(private readonly http: HttpClient) {}

	getAll() {
		return this.http.get<IGrade[]>(`${url}/groups`).pipe(untilDestroyed(this));
	}

	getGrade(id: string) {
		return this.http.get<IGrade>(`${url}/groups`).pipe(untilDestroyed(this));
	}

	create(data: IGrade) {
		return this.http.post<IGrade>(`${url}/groups`, { ...data }).pipe(untilDestroyed(this));
	}

	update(data: IGrade, id: string) {
		return this.http.patch<IGrade>(`${url}/groups/${id}`, { ...data }).pipe(untilDestroyed(this));
	}
}
