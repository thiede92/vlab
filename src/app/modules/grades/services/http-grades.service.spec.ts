import { TestBed } from "@angular/core/testing";

import { HttpGradesService } from "./http-grades.service";

describe("HttpGradesService", () => {
	let service: HttpGradesService;

	beforeEach(() => {
		TestBed.configureTestingModule({});
		service = TestBed.inject(HttpGradesService);
	});

	it("should be created", () => {
		expect(service).toBeTruthy();
	});
});
