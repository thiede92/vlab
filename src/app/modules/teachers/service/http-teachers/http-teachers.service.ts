import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { url } from "src/app/core/api-config";
import { IUser } from "../../../../core/models/user";

@Injectable({
	providedIn: "root",
})
export class HttpTeachersService {
	url = url;

	constructor(private readonly http: HttpClient) {}

	get(id: string) {
		return this.http.get<IUser>(`${this.url}/users/${id}`);
	}

	getAll() {
		return this.http.get<IUser>(`${this.url}/users/role/1`);
	}

	create(teacher: any) {
		return this.http.post<IUser>(`${this.url}/users`, { ...teacher, roleId: 1 });
	}

	update(teacher: any, id: string) {
		return this.http.put<IUser>(`${this.url}/users/${id}`, { ...teacher, roleId: 1 });
	}
}
