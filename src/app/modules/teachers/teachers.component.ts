import { Component, OnInit } from "@angular/core";
import { map } from "rxjs";
import { IColumnConfig } from "src/app/core/models/data-table";
import { IStudent } from "src/app/core/models/student";
import { HttpTeachersService } from "./service/http-teachers/http-teachers.service";

@Component({
	selector: "app-teachers",
	templateUrl: "./teachers.component.html",
	styleUrls: ["./teachers.component.scss"],
})
export class TeachersComponent implements OnInit {
	teachersColumnConfig: Array<IColumnConfig> = [
		{
			name: "email",
			columnName: "common.email",
		},
		{
			name: "firstName",
			columnName: "common.first-name",
		},
		{
			name: "lastName",
			columnName: "common.last-name",
		},
		{
			name: "groups",
			columnName: "common.group",
		},
		{
			name: "address",
			columnName: "common.address",
		},
		{
			name: "postCode",
			columnName: "common.post-code",
		},
		{
			name: "city",
			columnName: "common.city",
		},
		{
			name: "albumNumber",
			columnName: "common.album-number",
		},
	];
	teachersData: Array<IStudent> = [];
	teachersTableTitle: string = "table.teachers-list";

	constructor(private readonly httpTeachersService: HttpTeachersService) {
		this.getTeachers();
	}

	ngOnInit(): void {}

	getTeachers() {
		this.httpTeachersService
			.getAll()
			.pipe(
				map((data: any) => {
					return data.map((teacher: any) => {
						return {
							email: teacher.email,
							firstName: teacher.firstName,
							lastName: teacher.lastName,
							address: teacher.address,
							city: teacher.city,
							postCode: teacher.postCode,
							albumNumber: teacher.albumNumber,
							groups: "-",
						};
					});
				}),
			)
			.subscribe({
				next: (data: any) => (this.teachersData = data),
			});
	}
}
