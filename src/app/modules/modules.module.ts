import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ModulesRoutingModule } from "./modules-routing.module";
import { ModulesComponent } from "./modules.component";
import { SharedModule } from "../shared/shared.module";
import { SidebarComponent } from "../core/components/sidebar/sidebar.component";
import { AppToolbarComponent } from "../core/components/app-toolbar/app-toolbar.component";
import { BreadcrumbsComponent } from "../core/components/breadcrumbs/breadcrumbs.component";

@NgModule({
	declarations: [ModulesComponent, SidebarComponent, AppToolbarComponent, BreadcrumbsComponent],
	imports: [CommonModule, ModulesRoutingModule, SharedModule],
})
export class ModulesModule {}
