import { Component, HostListener, OnInit } from "@angular/core";
import { CdkDragDrop, moveItemInArray, transferArrayItem } from "@angular/cdk/drag-drop";
import { HttpGroupsService } from "./services/http-groups.service";
import { MatDialog } from "@angular/material/dialog";
import { GroupAddEditModalComponent } from "./group-add-edit-modal/group-add-edit-modal.component";
import { BehaviorSubject, delay, map, Observable, switchMap, tap } from "rxjs";
import { MatSnackBar } from "@angular/material/snack-bar";
import { TranslateService } from "@ngx-translate/core";
import { ComponentCanDeactivate } from "../../core/guards/unsaved-changes-guard/unsaved-changes.guard";
import { UntilDestroy, untilDestroyed } from "@ngneat/until-destroy";
import { ToastrService } from "ngx-toastr";
import { IGroup } from "../../core/models/group";
import { AuthService } from "../../core/services/auth/auth.service";
import { IUser } from "../../core/models/user";

@UntilDestroy()
@Component({
	selector: "app-groups",
	templateUrl: "./groups.component.html",
	styleUrls: ["./groups.component.scss"],
})
export class GroupsComponent implements OnInit, ComponentCanDeactivate {
	@HostListener("window:beforeunload", ["$event"])
	beforeUnloadHandler() {
		return !this.arrayChanged$.getValue();
	}
	arrayChanged$ = new BehaviorSubject<boolean>(false);
	myGroupsArray: IGroup[] = [];
	allGroupsArray: IGroup[] = [];
	currentUnsavedAmount!: string;
	remove: string = "common.remove";
	edit: string = "common.edit";
	allGroups$ = this.groupService.getAll();

	constructor(
		private groupService: HttpGroupsService,
		private readonly dialog: MatDialog,
		private snack: MatSnackBar,
		private translate: TranslateService,
		private toastr: ToastrService,
		private auth: AuthService,
	) {
		this.getData();
	}

	canDeactivate(): boolean | Observable<boolean> {
		if (this.arrayChanged$.getValue()) {
			return confirm(
				this.translate.instant("common.unsaved-changes-confirm") +
					". " +
					this.translate.instant("common.proceed-confirmation"),
			);
		} else {
			return true;
		}
	}

	ngOnInit(): void {}

	getData() {
		this.groupService
			.getAll()
			.pipe(untilDestroyed(this))
			.pipe(
				delay(1),
				map(data => {
					return data.filter(element => !this.myGroupsArray.includes(element));
				}),
			)
			.subscribe({
				next: data => {
					this.allGroupsArray = data;
				},
				error: err => {
					const error = this.translate.instant(err);

					this.toastr.error(error);
				},
			});

		this.auth.getProfile().subscribe(profile => {
			this.myGroupsArray = profile.groups;
		});
	}

	setData() {
		this.sortGroups();
		this.arrayChanged$.next(false);

		this.auth
			.getProfile()
			.pipe(
				switchMap((u: IUser) => {
					u.groups = this.myGroupsArray;

					return this.auth.updateUser(u, u.id);
				}),
			)
			.subscribe();
	}

	drop(event: CdkDragDrop<IGroup[]>) {
		if (event.previousContainer === event.container) {
			moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
			this.sortGroups();
		} else {
			transferArrayItem(
				event.previousContainer.data,
				event.container.data,
				event.previousIndex,
				event.currentIndex,
			);
			this.arrayChanged$.next(true);
		}
	}

	removeMyGroupItem(index: number) {
		this.allGroupsArray.push(this.myGroupsArray[index]);
		this.myGroupsArray.splice(index, 1);
		this.sortGroups();
		this.arrayChanged$.next(true);
	}

	addGroup() {
		const dialogRef = this.dialog.open(GroupAddEditModalComponent, {
			data: {
				title: "groups.add-group",
				newGroup: "",
			},
			width: "500px",
			panelClass: "p-4",
			autoFocus: false,
			restoreFocus: false,
			disableClose: false,
		});

		dialogRef
			.afterClosed()
			.pipe(
				switchMap(() => {
					return this.retrieve();
				}),
				tap(res => {
					if (!res) return this.arrayChanged$.next(false);
					return this.arrayChanged$.next(true);
				}),
			)
			.subscribe(result => {
				if (!result) return;
				this.allGroupsArray.push(...result);
			});
	}

	sortGroups() {
		this.allGroupsArray.sort();
		this.myGroupsArray.sort();
	}

	editGroup(id: string | undefined) {
		const dialogRef = this.dialog.open(GroupAddEditModalComponent, {
			data: {
				title: "groups.add-group",
				newGroup: "",
				id: id,
			},
			width: "500px",
			panelClass: "p-4",
			autoFocus: false,
			restoreFocus: false,
			disableClose: false,
		});

		dialogRef
			.afterClosed()
			.pipe(
				switchMap(() => {
					return this.retrieve();
				}),
				tap(res => {
					if (!res) return this.arrayChanged$.next(false);
					return this.arrayChanged$.next(true);
				}),
			)
			.subscribe(result => {
				if (!result) return;
				this.allGroupsArray = [];
				this.allGroupsArray.push(...result);
			});
	}

	retrieve() {
		this.allGroupsArray = [];
		this.arrayChanged$.next(false);
		return this.groupService.getAll();
	}
}
