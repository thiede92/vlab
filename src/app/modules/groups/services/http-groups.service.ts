import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { url } from "../../../core/api-config";
import { catchError, EMPTY } from "rxjs";
import { ErrorHandlerService } from "../../../core/services/error/error-handler.service";
import { IGroup } from "../../../core/models/group";

@Injectable({
	providedIn: "root",
})
export class HttpGroupsService {
	url = url;
	prefix = "groups";
	constructor(private http: HttpClient, private errorHandler: ErrorHandlerService) {}

	create(body: IGroup) {
		return this.http
			.post<IGroup>(`${this.url}/${this.prefix}`, body)
			.pipe(catchError(this.errorHandler.handleError));
	}

	getAll() {
		return this.http
			.get<IGroup[]>(`${this.url}/${this.prefix}`)
			.pipe(catchError(this.errorHandler.handleError));
	}

	getGroup(id: string) {
		if (!id) return EMPTY;

		return this.http
			.get<IGroup>(`${this.url}/${this.prefix}/${id}`)
			.pipe(catchError(this.errorHandler.handleError));
	}

	update(id: string, data: IGroup) {
		if (!id) return EMPTY;

		return this.http
			.patch<IGroup>(`${this.url}/${this.prefix}/${id}`, data)
			.pipe(catchError(this.errorHandler.handleError));
	}

	// getMyGroups() {
	// 	return this.http
	// 		.get<string[]>(`${this.url}/${this.prefix}`) //temporary: no endpoint
	// 		.pipe(catchError(this.errorHandler.handleError));
	// }

	// updateMyGroups(data: string[]) {
	// 	return this.http
	// 		.post<string[]>(`${this.url}/${this.prefix}`, data) //temporary: no endpoint
	// 		.pipe(catchError(this.errorHandler.handleError));
	// }
}
