import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { GroupsComponent } from "./groups.component";
import { UnsavedChangesGuard } from "../../core/guards/unsaved-changes-guard/unsaved-changes.guard";

const routes: Routes = [
	{ path: "", component: GroupsComponent, canDeactivate: [UnsavedChangesGuard] },
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule],
})
export class GroupsRoutingModule {}
