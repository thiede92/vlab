import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";

import {GroupsRoutingModule} from "./groups-routing.module";
import {GroupsComponent} from "./groups.component";
import {SharedModule} from "src/app/shared/shared.module";
import {PickListModule} from "primeng/picklist";
import {GroupAddEditModalComponent} from './group-add-edit-modal/group-add-edit-modal.component';

@NgModule({
	declarations: [GroupsComponent, GroupAddEditModalComponent],
	imports: [CommonModule, GroupsRoutingModule, SharedModule, PickListModule],
})
export class GroupsModule {}
