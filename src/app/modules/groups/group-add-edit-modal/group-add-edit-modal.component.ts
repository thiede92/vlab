import { Component, Inject, OnInit } from "@angular/core";
import { MAT_DIALOG_DATA } from "@angular/material/dialog";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { HttpGroupsService } from "../services/http-groups.service";
// @ts-ignore
import { UntilDestroy, untilDestroyed } from "@ngneat/until-destroy";
import { IGroup } from "../../../core/models/group";

@UntilDestroy()
@Component({
	selector: "app-group-add-edit-modal",
	templateUrl: "./group-add-edit-modal.component.html",
	styleUrls: ["./group-add-edit-modal.component.scss"],
})
export class GroupAddEditModalComponent implements OnInit {
	currentGroup!: IGroup;
	title!: string;
	form!: FormGroup;
	noChanges = true;

	constructor(
		@Inject(MAT_DIALOG_DATA)
		public data: {
			title: string;
			newGroup: string;
			id: string;
		},
		private readonly groupService: HttpGroupsService,
	) {
		this.title = data.title;
	}

	ngOnInit(): void {
		this.getGroupData();

		this.form = new FormGroup({
			name: new FormControl("", [Validators.required]),
			fieldOfStudy: new FormControl("", [Validators.required]),
			universityGroupCode: new FormControl("", [
				Validators.pattern(/^\d\w\d\d\d\w-\w+/),
				Validators.required,
			]),
			students: new FormControl([]), //temp like that dunno what backend want here
		});

		this.listenForChanges();
	}

	get name() {
		return this.form.get("name");
	}

	listenForChanges() {
		this.form.valueChanges.pipe(untilDestroyed(this)).subscribe(() => {
			this.noChanges = false;
		});
	}

	getGroupData() {
		if (!this.data.id) return;

		this.groupService
			.getGroup(this.data.id)
			.pipe(untilDestroyed(this))
			.subscribe(d => {
				this.currentGroup = d as IGroup;
				this.form.patchValue({ ...this.currentGroup });
			});
	}

	onSubmit() {
		if (this.data.id) {
			this.groupService.update(this.data.id, this.form.getRawValue()).subscribe();
		} else {
			this.groupService.create(this.form.getRawValue()).subscribe();
		}
	}
}
