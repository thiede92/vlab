import { trigger, animate, transition, style, group, query, stagger } from "@angular/animations";

export const slideInAnimation = trigger("slideInAnimation", [
	// Transition between any two states
	transition("* <=> *", [
		// Events to apply
		// Defined style and animation function to apply
		// Config object with optional set to true to handle when element not yet added to the DOM
		query(":enter, :leave", style({ position: "absolute", width: "100%", zIndex: 2 }), {
			optional: true,
		}),
		// group block executes in parallel
		group([
			query(
				":enter",
				[
					style([{ top: "-30px" }, { opacity: 0 }]),
					animate(".4s .1s ease-in-out", style([{ top: "0" }, { opacity: 1 }])),
				],
				{ optional: true },
			),
			query(
				":leave",
				[
					style([{ top: "0px" }, { opacity: 1 }]),
					animate("0s", style([{ top: "0" }, { opacity: 0 }])),
				],
				{ optional: true },
			),
		]),
	]),
]);

export const staggerdFadeIn = trigger("staggerdFadeIn", [
	transition("* => *", [
		query(
			":enter",
			[
				style([{ opacity: 0 }, { left: "50px" }]),
				stagger("100ms", [
					animate("700ms 200ms ease-in-out", style([{ opacity: 1 }, { left: "0" }])),
				]),
			],
			{ optional: true },
		),
		query(
			":leave",
			[
				style([{ opacity: 1 }, { left: "0" }]),
				stagger("100ms", [
					animate("200ms 200ms ease-in-out", style([{ opacity: 0 }, { left: "50px" }])),
				]),
			],
			{ optional: true },
		),
	]),
]);

export const listAnimation = trigger("listAnimation", [
	transition("* => *", [
		// each time the binding value changes
		query(":leave", [stagger(100, [animate("0.5s", style({ opacity: 0 }))])], { optional: true }),
		query(
			":enter",
			[style({ opacity: 0 }), stagger(100, [animate("0.5s", style({ opacity: 1 }))])],
			{
				optional: true,
			},
		),
	]),
]);
