import {AfterViewInit, Component} from "@angular/core";
import {TranslateService} from "@ngx-translate/core";
import {LoadingService} from "./core/services/loading/loading.service";
import {delay} from "rxjs";

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.scss"],
})
export class AppComponent implements AfterViewInit {
	title = "Virtual Laboratories";
	isLoading: boolean = false;

	constructor(translate: TranslateService, public loadingService: LoadingService) {
		const lang = window.localStorage.getItem("lang");
		if (lang) {
			translate.setDefaultLang(lang);
			translate.use(lang);
		} else {
			// this language will be used as a fallback when a translation isn't found in the current language
			translate.setDefaultLang("pl");
			// the lang to use, if the lang isn't available, it will use the current loader to get them
			translate.use("pl");
		}
	}

  ngAfterViewInit() {
    this.listenToLoading();
  }

	listenToLoading() {
		this.loadingService.isLoading.pipe(delay(0)).subscribe(isLoading => {
			this.isLoading = isLoading;
		});
	}
}
